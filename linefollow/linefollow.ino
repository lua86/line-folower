const int servo0 = 9;
const int servo1 = 10;
void setup() {
  // put your setup code here, to run once:
  pinMode(servo0, OUTPUT);
  pinMode(servo1, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
}


void loop() {
  // put your main code here, to run repeatedly:
  if (analogRead(A0) > 256) {
    left();
  } else if (analogRead(A1) > 256) {
    right();
  } else {
    forward();
  }
}

void forward() {
  analogWrite(servo0, 92);
  analogWrite(servo1, 230);  
}

void left() {
  analogWrite(servo0, 184);
  analogWrite(servo1, 230);
}

void right() {
  analogWrite(servo0, 92);
  analogWrite(servo1, 150);
}
