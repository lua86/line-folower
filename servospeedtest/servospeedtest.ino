const int servo0 = 9;
const int servo1 = 10;
void setup() {
  // put your setup code here, to run once:
pinMode(servo0, OUTPUT);
pinMode(servo1, OUTPUT);
pinMode(13, OUTPUT);
digitalWrite(13, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(servo0, 92);
  analogWrite(servo1, 230);
  delay(4000);
}
