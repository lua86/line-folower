const int servo0 = 9;
const int servo1 = 10;
void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  if (millis() % 4000 < 10) {
    forward();
  } else if (millis() % 2000 < 10) {
    left();
  } else if (millis() % 3000 < 10) {
    right();
  }
}



void forward() {
  analogWrite(servo0, 92);
  analogWrite(servo1, 230);  
}

void left() {
  analogWrite(servo0, 0);
  analogWrite(servo1, 230);
}

void right() {
  analogWrite(servo0, 92);
  analogWrite(servo1, 0);
}
